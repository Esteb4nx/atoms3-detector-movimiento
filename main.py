import os
import sys
import io
import time
import gc
import json
import network
import M5
from M5 import *
from hardware import *
from umqtt.simple import MQTTClient

wlan = None
mqtt_client = None
SSID = 'WIFI-DCI'
PASSWORD = 'DComInf_2K24'
THINGSBOARD_SERVER = 'iot.ceisufro.cl'
THINGSBOARD_PORT = 1883
ACCESS_TOKEN = 'dkerbmokdpt70rdxmw6e'

MOVEMENT_THRESHOLD = 0.2
QUIET_CONFIDENCE = 3

quiet_counter = 0 

def btnA_wasReleased_event(state):
    global wlan, mqtt_client
    pass

def connect_wifi(ssid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.disconnect()
    
    print('Conectando a la red...')
    wlan.connect(ssid, password)
    
    retry_count = 0
    while not wlan.isconnected() and retry_count < 10:
        print('Intentando conectar...')
        time.sleep(1)
        retry_count += 1
    
    if wlan.isconnected():
        print('Conectado a la red')
        return wlan
    else:
        print('No se pudo conectar a la red')
        return None

def connect_mqtt():
    global mqtt_client
    mqtt_client = MQTTClient(client_id="",
                             server=THINGSBOARD_SERVER,
                             port=THINGSBOARD_PORT,
                             user=ACCESS_TOKEN,
                             password="",
                             keepalive=60)
    mqtt_client.connect()
    print('Conectado al servidor MQTT de ThingsBoard')
    return mqtt_client

def send_data(data):
    global mqtt_client
    topic = 'v1/devices/me/telemetry'
    payload = json.dumps(data)
    try:
        mqtt_client.publish(topic, payload)
        print("Datos enviados: ", payload)
    except Exception as e:
        print("Error al enviar datos:", e)

def is_moving(accel_data):
    global quiet_counter

    x, y, z = accel_data
    magnitude = (x**2 + y**2 + z**2)**0.5
    moving = magnitude < 0.8 or magnitude > 1.2

    if not moving:
        quiet_counter += 1
        if quiet_counter >= QUIET_CONFIDENCE:
            return False
        else:
            return True
    else:
        quiet_counter = 0
        return True

def setup():
    global wlan, mqtt_client

    M5.begin()
    BtnA.setCallback(type=BtnA.CB_TYPE.WAS_RELEASED, cb=btnA_wasReleased_event)

    wlan = connect_wifi(SSID, PASSWORD)

    if wlan:
        mqtt_client = connect_mqtt()
        initial_data = {'datos': 'datitos'}
        send_data(initial_data)
        print("Prueba enviada")
    else:
        print("Error: No se pudo conectar a la red WiFi.")

def loop():
    global wlan, mqtt_client

    if wlan and wlan.isconnected():
        try:
            gyro_data = Imu.getGyro()
            accel_data = Imu.getAccel()
            print(accel_data)
            moving = is_moving(accel_data)
            print(moving)
            data = {
                "gyroscope-x": gyro_data[0],
                "gyroscope-y": gyro_data[1],
                "gyroscope-z": gyro_data[2],
                "accelerometer-x": accel_data[0],
                "accelerometer-y": accel_data[1],
                "accelerometer-z": accel_data[2],
                "moving": moving
            }
            send_data(data)
        except Exception as e:
            print("Error al enviar datos:", e)
    else:
        print("Error: No hay conexión WiFi.")
        wlan = connect_wifi(SSID, PASSWORD)
    
    gc.collect()

if __name__ == '__main__':
    try:
        setup()
        while True:
            loop()
    except (Exception, KeyboardInterrupt) as e:
        print("Error o interrupción:", e)
        sys.exit(1)
#include <WiFiManager.h>
#include <M5Unified.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

//Variables
const char* APssid = "Orthopedic sensor";
const char* deviceToken = "dkerbmokdpt70rdxmw6e";
const char* mqttServer = "iot.ceisufro.cl";
const char* restEndpoint = "v1/devices/me/telemetry";

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void createPayloadAndSend(float gyro[], float accel[], bool moving) {
  StaticJsonDocument<200> doc;

  doc["gyroscope-x"] = gyro[0];
  doc["gyroscope-y"] = gyro[1];
  doc["gyroscope-z"] = gyro[2];
  doc["accelerometer-x"] = accel[0];
  doc["accelerometer-y"] = accel[1];
  doc["accelerometer-z"] = accel[2];
  doc["moving"] = moving;

  char jsonBuffer[256];
  serializeJson(doc, jsonBuffer);
  //Serial.println(jsonBuffer);
  //M5.Lcd.printf("connected\nsending data");

  client.publish(restEndpoint, jsonBuffer);
}

void setup() {
    WiFi.mode(WIFI_STA);
    Serial.begin(115200);

    //AtomS3 Setup
    auto cfg = M5.config();
    M5.begin(cfg);
    M5.Lcd.setCursor(0,40);

    //WifiManager Setup
    WiFiManager wm;
    // wm.resetSettings(); //only for testing, disable in production

    //AP Setup
    M5.Lcd.printf("Connect to %s and visit 192.168.4.1", APssid);
    bool res;
    res = wm.autoConnect(APssid);
    M5.Lcd.setCursor(0,40);
    M5.Lcd.clear();
    delay(100); 

    //Check if wifi connected successfully
    if(!res) {
        Serial.println("Failed to connect");
        M5.Lcd.printf("Failed to connect");
        M5.Lcd.printf("Restarting...");
        delay(5000);
        ESP.restart();
    } 
    else {
        Serial.println("connected...yeey :)");
        M5.Lcd.printf("connected...yeey :)");
        delay(5000);
        client.setServer(mqttServer, 1883);
        client.setCallback(callback);
        reconnect();
    }

}

void loop() {
    if(!client.connected()){
      reconnect();
    }

    auto imu_update = M5.Imu.update();
    if (imu_update) {
        M5.Lcd.setCursor(0,40);
        M5.Lcd.clear();  // Delay 100ms 延迟100ms

        auto data = M5.Imu.getImuData();

        bool moving = isMoving(data.accel.value);
        createPayloadAndSend(data.gyro.value, data.accel.value, moving);
    }
    delay(50);
}

bool isMoving(float accel[]) {
  // Calculate magnitude of acceleration vector
  float accelMagnitude = sqrt(accel[0] * accel[0] + accel[1] * accel[1] + accel[2] * accel[2]);
  

  // Define a threshold for movement detection
  float topThreshold = 1.1; // Adjust this value according to your sensitivity needs
  float bottomThreshold = 0.9;
  // Check if acceleration exceeds the threshold
  return (accelMagnitude > topThreshold || accelMagnitude < bottomThreshold);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    // Attempt to connect
    if (client.connect("arduino-client", deviceToken, NULL)) {
      Serial.println("Connected to MQTT broker");
      
      // Subscribe to desired topics
      // client.subscribe("topic");
    } else {
      Serial.print("Failed, rc=");
      Serial.print(client.state());
      Serial.println("Trying again in 5 seconds");
      M5.Lcd.clear();
      M5.Lcd.printf("Failed to connect, trying again in 5 seconds.");
      
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.println("Message received: ");
  Serial.println(topic);

  // Handle incoming messages here
}